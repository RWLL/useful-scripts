# A simple script that activates a NodeJS enviroment

Due to me not haveing much disk space. I decided to put everything to do with NodeJS on a usb drive. I then ran into the problem of setting up the enviroment variables each time so I created this script to do it for me.

## Setup
Download the binary arcives of NodeJS for your platform and extract into the root of your device and rename the extracted folder to "node" then copy the activate script (from this repo) also to the root of the device.

# How to
Run "source activate" from the root of your device.
Now when you run node or npm you are running the binarys from node/bin/
To deactivate run deactivate_node (You do not have to be at the root of the device to ruun this.)
